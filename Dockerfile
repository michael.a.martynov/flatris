FROM node

RUN mkdir /app
WORKDIR /app

COPY package.json .
RUN yarn install

COPY . .
RUN yarn build

EXPOSE 3000

ENTRYPOINT yarn start
